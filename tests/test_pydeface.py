"""Testing for functions within pydeface_run.py script."""

import logging
import os
import shutil
import tempfile
from pathlib import Path
from unittest import mock
from unittest.mock import MagicMock, patch

import nibabel as nb
import numpy as np
import pytest

from fw_gear_dcm2niix.pydeface import pydeface_run

log = logging.getLogger(__name__)
ASSETS_DIR = Path(__file__).parent / "assets"
log.info(f"ASSETS_DIR: {ASSETS_DIR}")


@pytest.mark.parametrize(
    "with_template, with_facemask",
    [
        (False, False),
        (True, False),
        (False, True),
        (True, True),
    ],
)
def test_RunPydeface_Mocking_Outer(mocker, with_template, with_facemask):
    """
    Test number of calls of different functions using patching.
    Args:
        mocker: mocker object
        with_template (bool): whether or not to use template file
        with_facemask (bool): whether or not to use facemask file
    Returns:
        N/A, assertions are main point
    """

    # input files not already specified
    if with_template:
        template_file = f"{ASSETS_DIR}/pydeface_template.nii.gz"
    else:
        template_file = None
    if with_facemask:
        facemask_file = f"{ASSETS_DIR}/pydeface_facemask.nii.gz"
    else:
        facemask_file = None

    # set up
    infile = f"{ASSETS_DIR}/pydeface_T1.nii.gz"
    test_file_1 = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_1.nii.gz")
    test_file_2 = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_2.nii.gz")

    # patches
    run_pydeface_run_construct_log_command = mocker.patch(
        "fw_gear_dcm2niix.pydeface.pydeface_run.construct_log_command"
    )
    run_pydeface_run_deface_single_nifti = mocker.patch(
        "fw_gear_dcm2niix.pydeface.pydeface_run.deface_single_nifti"
    )
    run_pydeface_run_deface_multiple_niftis = mocker.patch(
        "fw_gear_dcm2niix.pydeface.pydeface_run.deface_multiple_niftis"
    )

    # construct command
    pydeface_run.construct_log_command(
        test_file_1,
        pydeface_cost="mutualinfo",
        template=template_file,
        facemask=facemask_file,
        pydeface_nocleanup=False,
        pydeface_verbose=False,
    )

    # checks
    run_pydeface_run_construct_log_command.assert_called_once()
    run_pydeface_run_deface_single_nifti.assert_not_called()
    run_pydeface_run_deface_multiple_niftis.assert_not_called()

    # deface
    pydeface_run.deface_multiple_niftis(
        [test_file_1, test_file_2],
        pydeface_cost="mutualinfo",
        template=template_file,
        facemask=facemask_file,
        pydeface_nocleanup=True,
        pydeface_verbose=True,
    )

    # checks
    run_pydeface_run_construct_log_command.assert_called_once()
    run_pydeface_run_deface_single_nifti.call_count == 2
    run_pydeface_run_deface_multiple_niftis.assert_called_once()

    # clean up
    os.remove(test_file_1)
    os.remove(test_file_2)


@pytest.mark.parametrize(
    "with_template, with_facemask",
    [
        (False, False),
        (True, False),
        (False, True),
        (True, True),
    ],
)
def test_RunPydeface_Mocking_Inner(
    mocker,
    with_template,
    with_facemask,
):
    """
    Args:
        mocker: mocker object
        with_template (bool): whether or not to use template file
        with_facemask (bool): whether or not to use facemask file
    Returns:
        N/A, assertions are main point
    """

    # input files not already specified
    if with_template:
        template_file = f"{ASSETS_DIR}/pydeface_template.nii.gz"
    else:
        template_file = None
    if with_facemask:
        facemask_file = f"{ASSETS_DIR}/pydeface_facemask.nii.gz"
    else:
        facemask_file = None

    # set up
    infile = f"{ASSETS_DIR}/pydeface_T1.nii.gz"
    test_file_1 = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_1.nii.gz")
    test_file_2 = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_2.nii.gz")

    # patch
    run_pydeface_run_deface_single_nifti = mocker.patch(
        "fw_gear_dcm2niix.pydeface.pydeface_run.deface_single_nifti"
    )

    # deface
    pydeface_run.deface_multiple_niftis(
        [test_file_1, test_file_2],
        pydeface_cost="mutualinfo",
        template=template_file,
        facemask=facemask_file,
        pydeface_nocleanup=True,
        pydeface_verbose=True,
    )

    # check
    assert run_pydeface_run_deface_single_nifti.call_count == 2

    # clean up
    os.remove(test_file_1)
    os.remove(test_file_2)


# @pytest.mark.parametrize(
#     "with_template, with_facemask, valid_file",
#     [
#         (False, False, f"{ASSETS_DIR}/pydeface_T1_infile.nii.gz"),
#         (True, False, f"{ASSETS_DIR}/pydeface_T1_template.nii.gz"),
#         (False, True, f"{ASSETS_DIR}/pydeface_T1_facemask.nii.gz"),
#         (True, True, f"{ASSETS_DIR}/pydeface_T1_alloptions.nii.gz"),
#     ],
# )
# def test_RunPydeface_Match(with_template, with_facemask, valid_file):
#     """
#     Args:
#         with_template (bool): whether or not to use template file
#         with_facemask (bool): whether or not to use facemask file
#         valid_file (str): hardcoded previous output to check against
#     Returns:
#         N/A, assertions are main point
#     """
#
#     # input files not already specified
#     if with_template:
#         template_file = f"{ASSETS_DIR}/pydeface_template.nii.gz"
#     else:
#         template_file = None
#     if with_facemask:
#         facemask_file = f"{ASSETS_DIR}/pydeface_facemask.nii.gz"
#     else:
#         facemask_file = None
#
#     # set up
#     infile = f"{ASSETS_DIR}/pydeface_T1.nii.gz"
#     test_file_i = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_i.nii.gz")
#     test_file_f = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test_f.nii.gz")
#
#     # run
#     pydeface_run.deface_multiple_niftis(
#         [test_file_f],
#         pydeface_cost="mutualinfo",
#         template=template_file,
#         facemask=facemask_file,
#         pydeface_nocleanup=False,
#         pydeface_verbose=False,
#     )
#     valid_image = nb.load(valid_file).get_fdata()
#     test_image_i = nb.load(test_file_i).get_fdata()
#     test_image_f = nb.load(test_file_f).get_fdata()
#
#     # check there was a change
#     change = (test_image_f - test_image_i).sum()
#     log.info(f"change: {change}")
#     assert abs(change) > 0
#     rel_change = abs(change / (test_image_i.sum()))
#     log.info(f"rel_change: {rel_change}")
#     assert rel_change >= 0.00001
#
#     # check result is what we've gotten before
#     outcome = np.array_equal(valid_image, test_image_f)
#     assert outcome == True
#
#     # TODO check the result was similar to what we know is a valid result:
#     # discrep = (test_image_f - valid_image).sum()
#     # rel_discrep = abs(discrep / valid_image.sum())
#     # assert rel_discrep >= 0.00001
#     # assert rel_discrep <= 0.001
#
#     # clean up
#     os.remove(test_file_i)
#     os.remove(test_file_f)


@pytest.mark.parametrize(
    "with_template, with_facemask",
    [
        (False, False),
        (True, False),
        (False, True),
        (True, True),
    ],
)
def test_RunPydeface_Log_Command_Check(with_template, with_facemask):
    """
    Args:
        with_template (bool): whether or not to use template file
        with_facemask (bool): whether or not to use facemask file
        valid_file (str): hardcoded previous output to check against
    Returns:
        N/A, assertions are main point
    """

    # input files not already specified
    if with_template:
        template_file = f"{ASSETS_DIR}/pydeface_template.nii.gz"
    else:
        template_file = None
    if with_facemask:
        facemask_file = f"{ASSETS_DIR}/pydeface_facemask.nii.gz"
    else:
        facemask_file = None

    # set up
    infile = f"{ASSETS_DIR}/pydeface_T1.nii.gz"
    test_file = shutil.copyfile(infile, f"{ASSETS_DIR}/pydeface_T1_test.nii.gz")

    # construct log command
    command, log_command = pydeface_run.construct_log_command(
        test_file,
        pydeface_cost="mutualinfo",
        template=template_file,
        facemask=facemask_file,
        pydeface_nocleanup=True,
        pydeface_verbose=True,
    )
    log.info(f"log_command: {log_command}")

    # check log_command
    assert (
        log_command
        == f"pydeface --outfile {str(test_file)} --force --cost mutualinfo"
        + (" --template " + template_file if with_template else "")
        + (" --facemask " + facemask_file if with_facemask else "")
        + f" --nocleanup --verbose {str(test_file)}"
    )

    # clean up
    os.remove(test_file)
