# Release notes

# 1.4.1_1.0.20211006

## Maintenance
- Publish to PyPi

# 1.4.0_1.0.20211006

## Fixes

* All outputs are not correctly saved when rescaling is happening

## Enhancements

* Gear default options now align with dcm2niix defaults. 
  Default filename changed to `%f_%p_%t_%s`. Default lossless scaling changed `original`. 

# 1.3.4_1.0.20211006

## Fixes
- Fix merge2d option argument.

## Maintenance
- Reformat release notes.

# 1.3.3_1.0.20211006

## Fixes
- Escape metacharacters on fname. Temporary fix nipype dcm2niix interface. Metacharacters ([]*) in fields used for output name don't break gear. @huiqiantan.
- Adjust check for sidecar to output image match. Switch metadata generation to naming fix as well. @naterichman.

## Enhancements
- Series Description is now optional.
- merge2d option default now set to auto (merge2d = "2").
- json sidecar now included by default (bids_sidecar = 'y').

## Maintenance
- Migrated to GitLab from [GitHub](https://github.com/flywheel-apps/dcm2niix).
- Transitioned to Poetry dependency management.
- Light refactoring of some wrapper code.


# 1.3.0_1.0.20201102

## What's Changed
- Changes to accept nested inputs @joinontrue (#9)
- Add contributing notes @NPann (#8)

## Impact On Gear Output
There should be none.


# 1.2.2_1.0.20201102

## What's Changed
- Refactor repository to make pip installable gear package. @margaretmahan (#7)

## Impact On Gear Output
None.


# 1.2.1_1.0.20201102

## Fixes
BUG: TypeError: Object of type 'bytes' is not JSON serializable. Occurs when the JSON sidecar produced by the dcm2niix tool contains bytes, which need to be decoded.
FIX: Resolve minor path issues.

## Enhancements
MAIN: Refactor script to use JSON sidecar as leading the metadata application, instead of based on image output files.
ENH: Add local tests directory with default config files for testing; in addition to a gear building script.

## Impact On Gear Output
None unless the user selected "ignore-errors" from the dcm2niix tool; then bypassing downstream issues with that has now been fixed.

## Availability
On the Exchange

## Upgrade Recommendation
Default functionality has not changed. Therefore, an upgrade is recommended on an as-needed basis.


# 1.2.0_1.0.20201102

## What's Changed

MAIN: Rename compress_nifti config option to compress_images.
MAIN: Cleanse and collate logic on ignoring errors and empty image output scenarios.
BUG: Fix the UnicodeDecodeError in metadata.py
BUG: Update logic to handle the case where bids_sidecar='o'
DOC: Update the gear label and gear description to explain inputs and outputs more clearly.
DOC: Clarify outputs given specific configuration options: compress_images, output_nrrd, etc.
DOC: Add a flowchart of the Gear workflow.

## Impact On Gear Output

NRRD output resolved when de-selecting compression configuration.


# 1.1.0_1.0.20201102

## What's Changed

ENH: Upgrade dcm2niix algorithm version to latest.
ENH: Incorporate additional dcm2niix command options including NRRD format, comments, and verbose logging.
DOC: Updates to README regarding metadata captured from this Gear.

## Impact On Gear Output

dcm2niix tool: The most applicable dcm2niix version upgrade includes fixes for BIDS RepetitionTimeExcitation and RepetitionTimeInversion tags (rordenlab/dcm2niix#439)

dcm2niix Gear: New output format, NRRD, available. Implementation of the configuration options to add comments and verbose logging.

# 1.0.0_1.0.20200331

Initial release of dcm2niix Flywheel Gear with PyDeface.
